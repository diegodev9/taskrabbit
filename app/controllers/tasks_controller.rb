class TasksController < ApplicationController
  before_action :set_task, only: %i[edit show update destroy]
  def index
    return @tasks = Task.all.order(created_at: :desc) unless params[:category].present?

    category = Category.find_by(name: params[:category])
    @tasks = Task.list_by_category(category.id)
  end

  def new
    @task = Task.new
  end

  def edit; end

  def show; end

  def create
    @task = Task.new(task_params)

    if @task.save
      redirect_to @task
    else
      render :new
    end
  end

  def update
    if @task.update(task_params)
      redirect_to @task
    else
      render :edit
    end
  end

  def destroy
    @task.destroy
    redirect_to root_path
  end

  private

  def set_task
    @task = Task.find(params[:id])
  end

  def task_params
    params.require(:task).permit(:title, :description, :company, :url, :category_id)
  end
end
