# == Schema Information
#
# Table name: tasks
#
#  id          :bigint           not null, primary key
#  company     :string
#  description :text
#  title       :string
#  url         :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :bigint
#
# Indexes
#
#  index_tasks_on_category_id  (category_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#
FactoryBot.define do
  factory :task do
    title { "MyString" }
    description { "MyText" }
    company { "MyString" }
    url { "MyString" }
  end
end
